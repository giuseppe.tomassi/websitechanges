#!/usr/bin/python3

import webChanges as we
import sys

if __name__ == "__main__":
    website = sys.argv[1] 
    email = [] + sys.argv[2:]
    print("Email will be sent to:")
    for e in email:
        print(e)
    i = input("Default time: 30s. Do you want to change it? [y] or [n]\n")
    if i == "y":
        try:
            time = float(input("Write how much time [s]\n"))
            we.check(website, email, float(time))
        except:
            print("Please, insert a number")
    elif i == "n":
        we.check(website, email)
