#!/usr/bin/python3

import requests as req
import time as t
import sendEmail as se

def downloadText(website):

    r = req.get(website).text
    return r

def check(website, email, time = 30.0):

    old = downloadText(website)
    new = old

    while (old == new):

        new = downloadText(website)
        print("[" + t.asctime() + "]" + "       --------------       OK")
        t.sleep(time)

    print("Qualcosa e' cambiato\n")

    se.sendEmail(email)
    print("Email " + str(i+1) + " has been sent")
